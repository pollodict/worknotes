# sequence.md

## hibernate_sequence

```sql
CREATE SEQUENCE hibernate_sequence MINVALUE 1000;
```

## base_sequence

```sql
CREATE SEQUENCE base_sequence MINVALUE 1000;
```

## import

```sql
CREATE SEQUENCE import_sequence MINVALUE 1000;
```
