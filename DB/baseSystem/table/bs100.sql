CREATE TABLE bs100 (
                objid NUMERIC(22,0) NOT NULL,
                version INTEGER,
                man_created VARCHAR(20) NOT NULL,
                date_created TIMESTAMP DEFAULT current_timestamp NOT NULL,
                man_last_updated VARCHAR(50),
                last_updated TIMESTAMP,
                note VARCHAR(50),
                system_type VARCHAR(20) NOT NULL,
                ptype VARCHAR(20) NOT NULL,
                ptype_desc VARCHAR(20) NOT NULL,
                CONSTRAINT bs100_pk PRIMARY KEY (objid)
);;
COMMENT ON TABLE bs100 IS '系統選單類別';
COMMENT ON COLUMN bs100.objid IS 'pk';
COMMENT ON COLUMN bs100.version IS '資料版本';
COMMENT ON COLUMN bs100.man_created IS '建檔人員';
COMMENT ON COLUMN bs100.date_created IS '建檔時間';
COMMENT ON COLUMN bs100.man_last_updated IS '最後異動人員';
COMMENT ON COLUMN bs100.last_updated IS '最後異動時間';
COMMENT ON COLUMN bs100.note IS '資料註記';
COMMENT ON COLUMN bs100.system_type IS '系統類別';
COMMENT ON COLUMN bs100.ptype IS '選單類別';
COMMENT ON COLUMN bs100.ptype_desc IS '選單說明';

CREATE INDEX bs100_idx1 ON BS100(PTYPE);
CREATE INDEX bs100_un1 ON BS100(SYSTEM_TYPE,PTYPE);