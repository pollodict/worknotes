-- DROP TABLE bs101;
CREATE TABLE bs101 (
                objid bigint  generated always AS identity,
                version INTEGER,
                man_created VARCHAR(20) NOT NULL,
                date_created TIMESTAMP DEFAULT current_timestamp NOT NULL,
                man_last_updated VARCHAR(50),
                last_updated TIMESTAMP,
                note VARCHAR(50),
                bs100_id NUMERIC(22,0) NOT NULL,
                ptype VARCHAR(20) NOT NULL,
                pcode INTEGER NOT NULL,
                pdesc VARCHAR(20) NOT NULL,
                exists_string1 VARCHAR(10),
                exists_string2 VARCHAR(10),
                exists_number1 INTEGER,
                exists_number2 INTEGER,
                CONSTRAINT bs101_pk PRIMARY KEY (objid)
);
COMMENT ON TABLE bs101 IS '系統選單';
COMMENT ON COLUMN bs101.objid IS 'pk';
COMMENT ON COLUMN bs101.version IS '資料版本';
COMMENT ON COLUMN bs101.man_created IS '建檔人員';
COMMENT ON COLUMN bs101.date_created IS '建檔時間';
COMMENT ON COLUMN bs101.man_last_updated IS '最後異動人員';
COMMENT ON COLUMN bs101.last_updated IS '最後異動時間';
COMMENT ON COLUMN bs101.note IS '資料註記';
COMMENT ON COLUMN bs101.ptype IS '選單類別';
COMMENT ON COLUMN bs101.pcode IS '選單代碼';
COMMENT ON COLUMN bs101.pdesc IS '選單代碼說明';
COMMENT ON COLUMN bs101.exists_string1 IS '補充文字1';
COMMENT ON COLUMN bs101.exists_string2 IS '系統選單.補充文字2';
COMMENT ON COLUMN bs101.exists_number1 IS '補充數字1';
COMMENT ON COLUMN bs101.exists_number2 IS '補充數字2';

CREATE INDEX bs101_idx1 ON BS101(PCODE);
CREATE INDEX bs101_idx2 ON BS101(PTYPE);

CREATE INDEX bs101_un1 ON BS101(PTYPE,PCODE);