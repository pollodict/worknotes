-- DROP TABLE bs_app_list_group;
CREATE TABLE bs_app_list_group (
                objid NUMERIC(22,0) NOT NULL,
                version INTEGER,
                man_created VARCHAR(20) NOT NULL,
                date_created TIMESTAMP DEFAULT current_timestamp NOT NULL,
                man_last_updated VARCHAR(20),
                last_updated TIMESTAMP,
                note VARCHAR(50),
                app_no CHAR(5) NOT NULL,
                app_group_no CHAR(5) NOT NULL,
                CONSTRAINT bs_app_list_group_pk PRIMARY KEY (objid)
);;
COMMENT ON TABLE bs_app_list_group IS '程式群組清單';
COMMENT ON COLUMN bs_app_list_group.objid IS 'pk';
COMMENT ON COLUMN bs_app_list_group.version IS '資料版本';
COMMENT ON COLUMN bs_app_list_group.man_created IS '建檔人員';
COMMENT ON COLUMN bs_app_list_group.date_created IS '建檔時間';
COMMENT ON COLUMN bs_app_list_group.man_last_updated IS '最後異動人員';
COMMENT ON COLUMN bs_app_list_group.last_updated IS '最後異動時間';
COMMENT ON COLUMN bs_app_list_group.note IS '資料註記';
COMMENT ON COLUMN bs_app_list_group.app_no IS '程式代號:前面兩個英文字母加三個阿拉伯數字組成[EN][###]';
COMMENT ON COLUMN bs_app_list_group.app_group_no IS '程式群組代號';


CREATE UNIQUE INDEX bs_app_list_group_idx1
 ON bs_app_list_group
 ( app_no, app_group_no );