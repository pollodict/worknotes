-- DROP TABLE bs_app_group;
CREATE TABLE bs_app_group (
    objid NUMERIC(22,0) NOT NULL,
    version INTEGER,
    man_created VARCHAR(20) NOT NULL,
    date_created TIMESTAMP DEFAULT current_timestamp NOT NULL,
    man_last_updated VARCHAR(20),
    last_updated TIMESTAMP,
    note VARCHAR(50),
    app_group_no CHAR(5) NOT NULL,
    app_group_name VARCHAR(10) NOT NULL,
    CONSTRAINT bs_app_group_pk PRIMARY KEY (objid)
);
COMMENT ON TABLE bs_app_group IS '程式群組';
COMMENT ON COLUMN bs_app_group.objid IS 'pk';
COMMENT ON COLUMN bs_app_group.version IS '資料版本';
COMMENT ON COLUMN bs_app_group.man_created IS '建檔人員';
COMMENT ON COLUMN bs_app_group.date_created IS '建檔時間';
COMMENT ON COLUMN bs_app_group.man_last_updated IS '最後異動人員';
COMMENT ON COLUMN bs_app_group.last_updated IS '最後異動時間';
COMMENT ON COLUMN bs_app_group.note IS '資料註記';
COMMENT ON COLUMN bs_app_group.app_group_no IS '程式群組代號';
COMMENT ON COLUMN bs_app_group.app_group_name IS '程式群組名稱';

CREATE UNIQUE INDEX bs_app_group_idx1
 ON bs_app_group
 ( app_group_no );