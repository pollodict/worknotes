-- drop TABLE bs_user ;
CREATE TABLE bs_user (
                objid NUMERIC(22,0) NOT NULL,
                version INTEGER,
                man_created VARCHAR(20) NOT NULL,
                date_created TIMESTAMP DEFAULT current_timestamp NOT NULL,
                man_last_updated VARCHAR(50),
                last_updated TIMESTAMP,
                note VARCHAR(50),
                username VARCHAR(20) NOT NULL,
                password VARCHAR(500) NOT NULL,
                enabled boolean DEFAULT true NOT NULL,
                account_expired boolean DEFAULT false NOT NULL,
                account_locked boolean DEFAULT false NOT NULL,
                password_expired boolean DEFAULT false NOT NULL,
                email VARCHAR(100),
                first_name VARCHAR(10),
                last_name VARCHAR(50) NOT NULL,
                CONSTRAINT bs_user_pk PRIMARY KEY (objid)
);
COMMENT ON TABLE bs_user IS '系統使用者帳號';
COMMENT ON COLUMN bs_user.objid IS 'user_id';
COMMENT ON COLUMN bs_user.version IS '資料版本';
COMMENT ON COLUMN bs_user.man_created IS '建檔人員';
COMMENT ON COLUMN bs_user.date_created IS '建檔時間';
COMMENT ON COLUMN bs_user.man_last_updated IS '最後異動人員';
COMMENT ON COLUMN bs_user.last_updated IS '最後異動時間';
COMMENT ON COLUMN bs_user.note IS '資料註記';
COMMENT ON COLUMN bs_user.username IS '帳號';
COMMENT ON COLUMN bs_user.password IS '密碼';
COMMENT ON COLUMN bs_user.enabled IS '帳戶是否可用';
COMMENT ON COLUMN bs_user.account_expired IS '帳戶是否過期';
COMMENT ON COLUMN bs_user.account_locked IS '帳戶是否被鎖定';
COMMENT ON COLUMN bs_user.password_expired IS '密碼是否過期';
COMMENT ON COLUMN bs_user.email IS '電子信箱';
COMMENT ON COLUMN bs_user.first_name IS '姓氏';
COMMENT ON COLUMN bs_user.last_name IS '名字';