CREATE TABLE bs_user_role (
                objid NUMERIC(22,0) NOT NULL,
                version INTEGER,
                man_created VARCHAR(20) NOT NULL,
                date_created TIMESTAMP DEFAULT current_timestamp NOT NULL,
                man_last_updated VARCHAR(50),
                last_updated TIMESTAMP,
                note VARCHAR(50),
                user_id NUMERIC(22,0) NOT NULL,
                role_id NUMERIC(22,0) NOT NULL,
                CONSTRAINT bs_user_role_pk PRIMARY KEY (objid)
);;
COMMENT ON TABLE bs_user_role IS '帳號與角色對表';
COMMENT ON COLUMN bs_user_role.objid IS 'user_id';
COMMENT ON COLUMN bs_user_role.version IS '資料版本';
COMMENT ON COLUMN bs_user_role.man_created IS '建檔人員';
COMMENT ON COLUMN bs_user_role.date_created IS '建檔時間';
COMMENT ON COLUMN bs_user_role.man_last_updated IS '最後異動人員';
COMMENT ON COLUMN bs_user_role.last_updated IS '最後異動時間';
COMMENT ON COLUMN bs_user_role.note IS '資料註記';
COMMENT ON COLUMN bs_user_role.user_id IS '帳號';
COMMENT ON COLUMN bs_user_role.role_id IS '角色';