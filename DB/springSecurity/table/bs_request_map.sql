-- DROP TABLE bs_request_map;
CREATE TABLE bs_request_map (
                objid NUMERIC(22,0) NOT NULL,
                version INTEGER,
                man_created VARCHAR(20) NOT NULL,
                date_created TIMESTAMP DEFAULT current_timestamp NOT NULL,
                man_last_updated VARCHAR(20),
                last_updated TIMESTAMP,
                note VARCHAR(50),
                app_no CHAR(5) NOT NULL,
                app_name VARCHAR(10) NOT NULL,
                idx INTEGER DEFAULT 0 NOT NULL,
                is_show BOOLEAN DEFAULT true NOT NULL,
                config_attribute VARCHAR(100) NOT NULL,
                http_method VARCHAR(50),
                url VARCHAR(50) NOT NULL,
                controller varchar(10),
                action varchar(10),
                CONSTRAINT bs_request_map_pk PRIMARY KEY (objid)
);;
COMMENT ON TABLE bs_request_map IS '程式清單暨系統角色可使用程式對應檔';
COMMENT ON COLUMN bs_request_map.objid IS 'pk';
COMMENT ON COLUMN bs_request_map.version IS '資料版本';
COMMENT ON COLUMN bs_request_map.man_created IS '建檔人員';
COMMENT ON COLUMN bs_request_map.date_created IS '建檔時間';
COMMENT ON COLUMN bs_request_map.man_last_updated IS '最後異動人員';
COMMENT ON COLUMN bs_request_map.last_updated IS '最後異動時間';
COMMENT ON COLUMN bs_request_map.note IS '資料註記';
COMMENT ON COLUMN bs_request_map.app_no IS '程式代號:前面兩個英文字母加三個阿拉伯數字組成[EN][###]';
COMMENT ON COLUMN bs_request_map.app_name IS '程式名稱';
COMMENT ON COLUMN bs_request_map.idx IS '排序';
COMMENT ON COLUMN bs_request_map.is_show IS '顯示';
COMMENT ON COLUMN bs_request_map.config_attribute IS '可使用角色';
COMMENT ON COLUMN bs_request_map.http_method IS '限制http_method';
COMMENT ON COLUMN bs_request_map.url IS 'url';
COMMENT ON COLUMN bs_request_map.controller IS 'controller';
COMMENT ON COLUMN bs_request_map.controller IS 'action';


CREATE UNIQUE INDEX bs_request_map_idx1 ON bs_request_map ( app_no );

CREATE UNIQUE INDEX bs_request_map_idx2 ON bs_request_map ( url );