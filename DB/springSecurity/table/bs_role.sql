CREATE TABLE bs_role (
                objid NUMERIC(22,0) NOT NULL,
                version INTEGER,
                man_created VARCHAR(20) NOT NULL,
                date_created TIMESTAMP DEFAULT current_timestamp NOT NULL,
                man_last_updated VARCHAR(50),
                last_updated TIMESTAMP,
                note VARCHAR(50),
                authority CHAR(10) NOT NULL,
                authority_name VARCHAR(20) NOT NULL,
                CONSTRAINT bs_role_pk PRIMARY KEY (objid)
);;
COMMENT ON TABLE bs_role IS '系統角色';
COMMENT ON COLUMN bs_role.objid IS 'user_id';
COMMENT ON COLUMN bs_role.version IS '資料版本';
COMMENT ON COLUMN bs_role.man_created IS '建檔人員';
COMMENT ON COLUMN bs_role.date_created IS '建檔時間';
COMMENT ON COLUMN bs_role.man_last_updated IS '最後異動人員';
COMMENT ON COLUMN bs_role.last_updated IS '最後異動時間';
COMMENT ON COLUMN bs_role.note IS '資料註記';
COMMENT ON COLUMN bs_role.authority IS '角色';
COMMENT ON COLUMN bs_role.authority_name IS '角色說明';