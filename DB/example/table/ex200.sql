-- DROP TABLE ex200;
CREATE TABLE ex200 (
                objid NUMERIC(22,0) NOT NULL,
                version INTEGER,
                man_created VARCHAR(20) NOT NULL,
                date_created TIMESTAMP DEFAULT current_timestamp NOT NULL,
                man_last_updated VARCHAR(50),
                last_updated TIMESTAMP,
                note VARCHAR(50),
                title VARCHAR(20) NOT NULL,
                content VARCHAR(100),
                CONSTRAINT ex200_pk PRIMARY KEY (objid)
);;
COMMENT ON TABLE ex200 IS '主檔';
COMMENT ON COLUMN ex200.objid IS 'pk';
COMMENT ON COLUMN ex200.version IS '資料版本';
COMMENT ON COLUMN ex200.man_created IS '建檔人員';
COMMENT ON COLUMN ex200.date_created IS '建檔時間';
COMMENT ON COLUMN ex200.man_last_updated IS '最後異動人員';
COMMENT ON COLUMN ex200.last_updated IS '最後異動時間';
COMMENT ON COLUMN ex200.note IS '資料註記';
COMMENT ON COLUMN ex200.title IS '標題';
COMMENT ON COLUMN ex200.content IS '內容';