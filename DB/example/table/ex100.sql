--DROP TABLE ex100;
CREATE TABLE ex100 (
                objid NUMERIC(22,0) NOT NULL,
                version INTEGER,
                man_created VARCHAR(20) NOT NULL,
                date_created TIMESTAMP DEFAULT current_timestamp NOT NULL,
                man_last_updated VARCHAR(20),
                last_updated TIMESTAMP,
                note VARCHAR(50),
                strings VARCHAR(50),
                integers INTEGER,
                ATMS INTEGER,
                status INTEGER DEFAULT 0,
                article TEXT,
                CONSTRAINT ex100_pk PRIMARY KEY (objid)
);;
COMMENT ON TABLE ex100 IS '範例表單';
COMMENT ON COLUMN ex100.objid IS 'pk';
COMMENT ON COLUMN ex100.version IS '資料版本';
COMMENT ON COLUMN ex100.man_created IS '建檔人員';
COMMENT ON COLUMN ex100.date_created IS '建檔時間';
COMMENT ON COLUMN ex100.man_last_updated IS '最後異動人員';
COMMENT ON COLUMN ex100.last_updated IS '最後異動時間';
COMMENT ON COLUMN ex100.note IS '資料註記';
COMMENT ON COLUMN ex100.strings IS '字串';
COMMENT ON COLUMN ex100.integers IS '數字';
COMMENT ON COLUMN ex100.ATMS IS '金額';
COMMENT ON COLUMN ex100.status IS '流程狀態
0: 無狀態
BS101: ex100_stasus';
COMMENT ON COLUMN ex100.article IS '文章';