-- DROP TABLE ex210;
CREATE TABLE ex210 (
                objid NUMERIC(22,0) NOT NULL,
                version INTEGER,
                man_created VARCHAR(20) NOT NULL,
                date_created TIMESTAMP DEFAULT current_timestamp NOT NULL,
                man_last_updated VARCHAR(50),
                last_updated TIMESTAMP,
                note VARCHAR(50),
                ex200_id NUMERIC(22,0) NOT NULL,
                contents VARCHAR(100),
                CONSTRAINT ex210_pk PRIMARY KEY (objid)
);
COMMENT ON TABLE ex210 IS '子表';
COMMENT ON COLUMN ex210.objid IS 'pk';
COMMENT ON COLUMN ex210.version IS '資料版本';
COMMENT ON COLUMN ex210.man_created IS '建檔人員';
COMMENT ON COLUMN ex210.date_created IS '建檔時間';
COMMENT ON COLUMN ex210.man_last_updated IS '最後異動人員';
COMMENT ON COLUMN ex210.last_updated IS '最後異動時間';
COMMENT ON COLUMN ex210.note IS '資料註記';
COMMENT ON COLUMN ex210.ex200_id IS 'ex200_id';
COMMENT ON COLUMN ex210.contents IS '內容';