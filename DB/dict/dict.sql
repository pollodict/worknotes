create MATERIALIZED VIEW IF NOT EXISTS dict as
select
(select t.word from dict100 t where t.objid = d.dict100_id) phrase,
(select t.bopomofo from dict300 t where t.objid = d.dict300_id) bopomofo
from dict100_300 d
union
select
(select t.phrase from dict200 t where t.objid = d.dict200_id) phrase,
(select t.bopomofo from dict300 t where t.objid = d.dict300_id) bopomofo
from dict200_300 d
;


REFRESH MATERIALIZED VIEW dict;

