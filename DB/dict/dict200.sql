-- DROP TABLE IF EXISTS dict200;
CREATE TABLE dict200(
    OBJID NUMERIC(22) NOT NULL,
    VERSION INT4 DEFAULT  0,
    MAN_CREATED VARCHAR(32) NOT NULL,
    DATE_CREATED TIMESTAMP NOT NULL DEFAULT  current_timestamp,
    MAN_LAST_UPDATED VARCHAR(32),
    LAST_UPDATED TIMESTAMP DEFAULT  current_timestamp,
    phrase VARCHAR(50) NOT NULL,
    phrase_other VARCHAR(50),
    phrase_count INTEGER NOT NULL DEFAULT  0,
    phrase_no CHAR(10) NOT NULL,
    phrase_class char(1) NOT NULL,
    total_number INTEGER NOT NULL DEFAULT  0,
    other_number INTEGER NOT NULL DEFAULT  0,
    ing_order VARCHAR(20) NOT NULL DEFAULT  0,
    bopomofo1 VARCHAR(20) NOT NULL,
    bopomofo_type INTEGER,
    bopomofo2 VARCHAR(20),
    bopomofo3 VARCHAR(20),
    bopomofo4 VARCHAR(20),
    like_phrase VARCHAR(20),
    unlike_phrase VARCHAR(20),
    means1 varchar(200),
    means2 varchar(200),
    phrase2 VARCHAR(20),
    PRIMARY KEY (OBJID)
);

COMMENT ON TABLE dict200 IS '詞彙';
COMMENT ON COLUMN dict200.OBJID IS 'PK';
COMMENT ON COLUMN dict200.VERSION IS '資料版本';
COMMENT ON COLUMN dict200.MAN_CREATED IS '建檔人員';
COMMENT ON COLUMN dict200.DATE_CREATED IS '建檔時間';
COMMENT ON COLUMN dict200.MAN_LAST_UPDATED IS '最後異動人員';
COMMENT ON COLUMN dict200.LAST_UPDATED IS '最後異動時間';
COMMENT ON COLUMN dict200.phrase IS '詞組';
COMMENT ON COLUMN dict200.phrase_other IS '辭條別名';
COMMENT ON COLUMN dict200.phrase_count IS '字數';
COMMENT ON COLUMN dict200.phrase_no IS '字詞號';
COMMENT ON COLUMN dict200.phrase_class IS '部首字';
COMMENT ON COLUMN dict200.total_number IS '總筆畫數';
COMMENT ON COLUMN dict200.other_number IS '部首外筆畫數';
COMMENT ON COLUMN dict200.ing_order IS '多音排序';
COMMENT ON COLUMN dict200.bopomofo1 IS '注音一式';
COMMENT ON COLUMN dict200.bopomofo_type IS '變體類型;1:變 2:又音 3:語音 4:讀音';
COMMENT ON COLUMN dict200.bopomofo2 IS '變體注音';
COMMENT ON COLUMN dict200.bopomofo3 IS '漢語拼音';
COMMENT ON COLUMN dict200.bopomofo4 IS '漢語拼音數字';
COMMENT ON COLUMN dict200.like_phrase IS '相似詞';
COMMENT ON COLUMN dict200.unlike_phrase IS '相反詞';
COMMENT ON COLUMN dict200.means1 IS '釋義';
COMMENT ON COLUMN dict200.means2 IS '多音參見訊息';
COMMENT ON COLUMN dict200.phrase2 IS '異體字';
