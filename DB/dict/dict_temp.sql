-- DROP TABLE IF EXISTS dict_temp;
CREATE TABLE dict_temp(
    OBJID NUMERIC(22) NOT NULL DEFAULT  nextval('import_sequence'),
    phrase VARCHAR(50) NOT NULL,
    phrase_other VARCHAR(100),
    phrase_count INTEGER NOT NULL DEFAULT  0,
    phrase_no CHAR(20) NOT NULL,
    phrase_class char(1) NOT NULL,
    total_number INTEGER NOT NULL DEFAULT  0,
    other_number INTEGER NOT NULL DEFAULT  0,
    ing_order VARCHAR(20) NOT NULL DEFAULT  0,
    bopomofo1 VARCHAR(100) NOT NULL,
    bopomofo_type VARCHAR(20),
    bopomofo2 VARCHAR(100),
    bopomofo3 VARCHAR(100),
    bopomofo4 VARCHAR(100),
    like_phrase VARCHAR(500),
    unlike_phrase VARCHAR(500),
    means1 text,
    means2 text,
    phrase2 VARCHAR(20),
    PRIMARY KEY (OBJID)
);

COMMENT ON TABLE dict_temp IS '字詞匯入暫存';
COMMENT ON COLUMN dict_temp.OBJID IS 'PK';
COMMENT ON COLUMN dict_temp.phrase IS '詞組';
COMMENT ON COLUMN dict_temp.phrase_other IS '辭條別名';
COMMENT ON COLUMN dict_temp.phrase_count IS '字數';
COMMENT ON COLUMN dict_temp.phrase_no IS '字詞號';
COMMENT ON COLUMN dict_temp.phrase_class IS '部首字';
COMMENT ON COLUMN dict_temp.total_number IS '總筆畫數';
COMMENT ON COLUMN dict_temp.other_number IS '部首外筆畫數';
COMMENT ON COLUMN dict_temp.ing_order IS '多音排序';
COMMENT ON COLUMN dict_temp.bopomofo1 IS '注音一式';
COMMENT ON COLUMN dict_temp.bopomofo_type IS '變體類型;1:變 2:又音 3:語音 4:讀音';
COMMENT ON COLUMN dict_temp.bopomofo2 IS '變體注音';
COMMENT ON COLUMN dict_temp.bopomofo3 IS '漢語拼音';
COMMENT ON COLUMN dict_temp.bopomofo4 IS '漢語拼音數字';
COMMENT ON COLUMN dict_temp.like_phrase IS '相似詞';
COMMENT ON COLUMN dict_temp.unlike_phrase IS '相反詞';
COMMENT ON COLUMN dict_temp.means1 IS '釋義';
COMMENT ON COLUMN dict_temp.means2 IS '多音參見訊息';
COMMENT ON COLUMN dict_temp.phrase2 IS '異體字';
